from django.db import models
from django.contrib.auth.models import User, AbstractUser
from . import global_vars
from django.template.defaultfilters import slugify


class Usuario(AbstractUser):

    class Meta:
        ordering = ['username']

    slug = models.SlugField()

    #confirm_password = models.CharField(max_length=60, blank=True, null=True)

    birth_date = models.DateField()

    imagen = models.ImageField(upload_to='usuarios/img/profile', default=global_vars.DEFAULT_USER_IMAGE)

    tipos_id = (
        ('CC', 'Cédula de Ciudadanía'),
        ('TI', 'Tarjeta de Identidad'),
        ('CE', 'Cédula de Extrangería'),
        ('PS', 'Pasaporte'),
    )

    tipo_id = models.CharField(max_length=2, choices=tipos_id, default='CC',
                               help_text="Identificador del tipo de identificación",
                               )
    identificacion = models.CharField(max_length=20, help_text="Número identificación")

    lugar_residencia = models.CharField(max_length=60, blank=True, null=True, default=None)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.username)
        self.set_password(self.password)
        super(Usuario, self).save(*args, **kwargs)

    def __str__(self):
        return self.username
