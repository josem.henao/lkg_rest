from django.contrib.auth import authenticate
from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_401_UNAUTHORIZED, HTTP_404_NOT_FOUND

from usuarios.models import Usuario
from usuarios.serializers import UserSerializer, GroupSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Usuario.objects.all().order_by('date_joined')
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = (IsAuthenticated,)


@api_view(["POST"])
def login(request):
    print(request.data)
    username = request.data.get("username")
    password = request.data.get("password")

    user = authenticate(username=username, password=password)
    if not user:
        return Response({"error": "Login failed"}, status=HTTP_401_UNAUTHORIZED)

    token, _ = Token.objects.get_or_create(user=user)
    return Response({"token": token.key})

@api_view(["POST"])
def logout(request):
    print(request.data)
    try:
        username = request.data['username']
        user = Usuario.objects.get(username=username)
    except:
        msg = "El nombre de usuario no está registrado en las bases de datos"
        #raise ObjectDoesNotExist(msg)
        return Response({"error" : msg}, status=HTTP_404_NOT_FOUND)

    if user:
        try:
            token = Token.objects.get(user_id=user.id)
        except:
            msg = "El usuario indicado no tiene generado un Token"
            #raise ObjectDoesNotExist(msg)
            return Response({"error":msg}, status=HTTP_404_NOT_FOUND)
        if token:
            if token == request._auth:
                token.delete()
                return Response({"detail" : "logged out"})
            else:
                msg = "El Token enviado no coresponde al Token almacenado para el usuario indicado"
                #raise ValidationError(msg)
                return Response({"error":msg},status=HTTP_401_UNAUTHORIZED)