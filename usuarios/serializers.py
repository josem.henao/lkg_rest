from django.contrib.auth.models import Group
from usuarios.models import Usuario
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Usuario
        fields = (
            'url','id', 'last_login', 'password', 'username', 'first_name', 'last_name', 'email', 'date_joined',
            'birth_date', 'imagen',
            'tipo_id', 'identificacion', 'lugar_residencia', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')
